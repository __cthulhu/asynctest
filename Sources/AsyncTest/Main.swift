//
//  File.swift
//  
//
//  Created by Anton on 26.02.2022.
//

import Foundation
import Networking

@main
struct Main {
    static func main() async throws {
        try await Service().getModels()
    }
}

func doTest() {
    print("")
    Task {
        let service = Service()
        do {
            let data = try await service.getModels()
            print(data)
        } catch {
            print(error)
        }
    }
}

struct Model: Decodable {
    let userId: Int
    let id: Int
    let title: String
    let completed: Bool
}

//@MainActor
final class Service {
    func getModels() async throws -> [Model] {
        print("AAAAA")
        return try await BasicRequest<EmptyBody, [Model]>("https://jsonplaceholder.typicode.com/todos/").execute()
    }
}
