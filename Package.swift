// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AsyncTest",
    platforms: [
       .macOS(.v10_15)
    ],
    products: [
        .executable(name: "AsyncTest", targets: ["AsyncTest"])
    ],
    dependencies: [
        .package(path: "../NXKit/NXKit/NXKit/NXKit/Sources/Common"),
        .package(path: "../NXKit/NXKit/NXKit/NXKit/Sources/Networking")
    ],
    targets: [
        .executableTarget(name: "AsyncTest",
                dependencies: ["Common", "Networking"]),
    ],
    swiftLanguageVersions: [
        .v5
    ]
)
